import Axios from 'axios'

let axiosInstance = Axios.create({
  baseURL: `${process.env.API_URL}/api/`
})

export default ({ Vue }) => {
  Vue.prototype.$axios = Axios
}

export { axiosInstance }