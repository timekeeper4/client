export default {
  dateFormat: 'YYYY-MM-DD',

  dateTimeFormat: 'YYYY-MM-DD HH:mm A',

  fullDateTimeFormat: 'MMMM DD YYYY HH:mm:ss',

  timeFormat: 'hh:mm A',

  tokenKey: 'token'
}
