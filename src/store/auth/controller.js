export default {
  setUser: ({ commit }, res) => {
    commit('SET_USER', res.data)
    return Promise.resolve(res)
  },
  validate: ({ commit }, res) => {
    commit("SET_USER", res.data)
    return Promise.resolve(!!res.data)
  },
  logout: ({ commit }) => {
    commit("SET_USER", null)
    return Promise.resolve(null)
  }
}