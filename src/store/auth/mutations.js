import { setToken, removeToken, getToken } from '../../utils/auth'

const mutations = {
  SET_USER (state, payload) {
    let token = getToken()

    if (!token && payload) {
      token = payload.token
    }
    
    payload ? setToken(token) : removeToken()
    state.token = payload && token ? token : null
    state.user = payload ? payload.user : {}
    state.role = payload && payload.user.role ? payload.user.role : {}
  }
}

export default mutations