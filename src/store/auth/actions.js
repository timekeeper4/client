import { axiosInstance } from "boot/axios";
import { getToken, setToken } from 'src/utils/auth'

import Controller from './controller'

let actions = {
  login: (ctx, data) => axiosInstance.post('auth/login', data)
  .then(result => Controller.setUser(ctx, result))
  .catch(err => { return err }),
  validate: (ctx, data) => {
    let token = getToken()
    return axiosInstance.get('auth/validate', { headers: { authorization: token } })
    .then(result => {
      let { data } = result
      let { user } = data || {}

      if (data && user && user.role.accessLevel == 4 && data.token) {
        setToken(data.token)
        user.company ? Controller.setCompany(ctx, user.company) : null
      }

      Controller.validate(ctx, result)
      return result.data
    })
    .catch(err => Controller.logout(ctx, null))
  },
  logout: (ctx, data) => Controller.logout(ctx, data)
}

export default actions