const getters = {
  isLogin: state => !!state.token,
  user: state => state.user,
  role: state => state.user.role,
}

export default getters