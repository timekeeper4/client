import { axiosInstance } from "src/boot/axios";
import { getToken } from 'src/utils/auth'

let token = getToken()

let actions = {
  paginate: (ctx, data) => axiosInstance.get('timelog/paginate', { params: data, headers: { authorization: getToken() } }),
  find: (ctx, data) => axiosInstance.get(`timelog/${data.id}`, { headers: { authorization: getToken() } }),
  create: (ctx, data) => axiosInstance.post(`timelog`, data, { headers: { authorization: token } }),
  update: (ctx, data) => axiosInstance.put(`timelog/${data.id}`, {}, { headers: { authorization: token } }),
}

export default actions