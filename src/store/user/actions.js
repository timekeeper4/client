import { axiosInstance } from "src/boot/axios";
import { getToken } from 'src/utils/auth'

let token = getToken()

let actions = {
  forApprovalUser: (ctx, data) => axiosInstance.get('user/paginate', { params: data, headers: { authorization: getToken() } }),
  approvedUser: (ctx, data) => axiosInstance.put(`user/${data.id}`, {}, { headers: { authorization: token } }),
  registerUser: (ctx, data) => axiosInstance.post(`user`, data, { headers: { authorization: token } }),
}

export default actions