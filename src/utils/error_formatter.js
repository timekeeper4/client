import { showErrorNotify } from "./notification";

export const errorMessage = (fields, err) => {
  let errorObj = err || {}
  let responseObj = errorObj.response || {}
  let dataObj = responseObj.data || {}
  let errors = dataObj.errors ?? dataObj.message

  if (typeof errors == 'object') {
    let errKeys = typeof errors == 'object' ? Object.keys(errors) : []
    let arr_fields = fields || []
    let err_message
    let obj_key
  
    obj_key = errKeys.filter(item => arr_fields.some(val => val === item))
  
    if (obj_key.length > 1) {
      err_message = obj_key.map(item => errors[item][0]) 
    }
    else if (obj_key.length === 1) {
      let single_key = obj_key[0]
      err_message = errors[single_key][0]
    }
    else err_message = errors
  
    if (err_message && typeof err_message === 'string') {
      return showErrorNotify(err_message)
    } else if (err_message && err_message.length > 0) {
      return err_message.forEach(item => showErrorNotify(item))
    } else {
      for (let key in err_message) {
        if (err_message[key].length === 1)
          showErrorNotify(err_message[key][0])
        else
          showErrorNotify(err_message[key])
      }
    }
  }
  else {
    showErrorNotify(errors)
  }
}