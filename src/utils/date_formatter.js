import { date } from 'quasar'
import Setting from 'src/setting'

export const date_format = value => {
    return date.formatDate(new Date(value), Setting.dateFormat)
}

export const time_format = value => {
    return date.formatDate(new Date(value), Setting.timeFormat)
}

export const date_time_format = value => {
    return date.formatDate(new Date(value), Setting.dateTimeFormat)
}