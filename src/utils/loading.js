import { Loading, QSpinnerHourglass } from 'quasar'

export const showLoading = message => {
    return Loading.show({
        spinner: QSpinnerHourglass,
        message
    })
}

export const hideLoading = () => Loading.hide()