import { Notify } from 'quasar'

Notify.setDefaults({
    position: 'top',
    timeout: 1500,
    textColor: 'white',
    classes: ['q-px-lg']
})

export const showSuccessNotify = (message, notifType) => {
    return Notify.create({
        message,
        type: notifType || 'positive'
    })
}

export const showErrorNotify = (message, notifType) => {
    return Notify.create({
        message,
        type: notifType ||'danger'
    })
}

export const showTimeLogNotify = (message, notifType) => {
    return Notify.create({
        timeout: 0,
        type: notifType || 'danger',
        message,
        closeBtn: 'X',
        position: 'center',
        classes: ['text-h4 q-pa-lg']
    })
}