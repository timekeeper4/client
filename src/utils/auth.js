import config from 'src/setting'

const tokenKey = config.tokenKey

export function getToken() {
  return localStorage.getItem(tokenKey)
}

export function setToken(token) {
  return localStorage.setItem(tokenKey, token)
}

export function removeToken() {
  return localStorage.removeItem(tokenKey)
}

export function setReserveCount (value) {
  return localStorage.setItem('count', value)
}

export function getReserveCount () {
  return localStorage.getItem('count')
}