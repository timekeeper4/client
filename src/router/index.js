import Vue from 'vue'
import VueRouter from 'vue-router'

import routes from './routes'

Vue.use(VueRouter)

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default function ({ store }) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })

  Router.beforeEach(async (to, from, next) => {
    const isAuthenticated = store.getters['Auth/isLogin']
    const userData = store.getters['Auth/user']
    if (to.matched.some(record => record.meta.authRequired)) {
      if (!isAuthenticated) {
        next({ path: '/login' })
      } else {
        return store.dispatch('Auth/validate')
        .then(validateUser => {
          let accesstype = validateUser.user ? validateUser.user.role.accessLevel == 1 ? 'admin' : validateUser.user.role.accessLevel == 2 ? 'employee' : undefined : undefined
          if (to.meta.access) {  
            if (!to.meta.access.includes(accesstype)) {
              next('error404')
            }
          }
          return validateUser ? next() : next({ path: '/login' })
        })
      }
    }
    else if (to.matched.some(record => record.meta.auth)) {
      if (isAuthenticated)
        next({ path: '/' })
      else
        next()
    }
    else if (to.matched.some(record => record.meta.isApproved)) {
      if (userData) {
        if (to.meta.registerLevel.length) {
          if (!to.meta.registerLevel.includes(userData.registrationLevel)) {
            next('error404')
          }
          next()
        }
      }
      else
        next('error404')
    }
    else {
      next()
    }
  })

  return Router
}
