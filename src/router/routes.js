
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    meta: { authRequired: true },
    children: [
      { 
        path: '/', 
        component: () => import('pages/Dashboard.vue'),
        meta: { access: ['admin', 'employee'] }
      },
      { 
        name: 'logs',
        path: '/logs', 
        component: () => import('pages/Index.vue'),
        meta: { access: ['admin'] }
      }
    ]
  },
  {
    path: '/login',
    component: () => import('src/pages/Login.vue'),
    meta: { auth: true }
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
